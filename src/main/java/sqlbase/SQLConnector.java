package sqlbase;


import model.Exchange;
import model.Game;
import org.json.JSONObject;

import java.sql.*;
import java.util.*;

/**
 * Created by Ivametal on 17.03.2017.
 */
public class SQLConnector {

    private static SQLConnector sqlConnector = new SQLConnector();
    public static final String EXCHANGE_TABLE = "exchanges";
    public static final String XBOX_TABLE = "xbox";
    public static final String PSFOUR_TABLE = "psfour";
    public static final String PSTHREE_TABLE = "psthree";
    public static final String STEAM_TABLE = "steam";
    private static final String UPDATE_TABLE = "current_update";

    private static final String url = "jdbc:mysql://localhost:3306/sales?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    private static final String user = "root";
    private static final String password = "11091998";

    private Connection connection;
    private Statement statement;
    private ResultSet set;



    private SQLConnector () {

        try {
            connection = DriverManager.getConnection(url, user, password);
            statement = connection.createStatement();
            statement.executeQuery("USE sales;");
            statement.executeUpdate("CREATE TABLE IF NOT EXISTS current_update (update_date VARCHAR(16));");

//            remakeTable(PSFOUR_TABLE);
//            remakeTable(PSTHREE_TABLE);
//            remakeTable(STEAM_TABLE);
//            remakeTable(XBOX_TABLE);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public void updateTableAfterPost(String json) {
        JSONObject jsonObject = new JSONObject(json);
        if (jsonObject.getInt("remove") == 1) {
            deleteRowFromExchangeTable(jsonObject);
            return;
        }
        updateDatabase(jsonObject);
    }

    public boolean isUpdateNeeded() {
        try {
            ResultSet rs = statement.executeQuery("SELECT * FROM current_update;");
            rs.last();
            if (rs.getRow()==0)
                return true;
            else {
                long difference = System.currentTimeMillis() - Long.parseLong(rs.getString("update_date").replaceAll(" ",""));
                if (difference<86400000) {
                    System.out.println(difference);
                    return false;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

    public static SQLConnector getSqlConnector() {
        return sqlConnector;
    }

    public void deleteRowFromExchangeTable(JSONObject jsonObject) {
        String vkid = jsonObject.getInt("vkid")+"";
        System.out.println(vkid);
        try {
            statement.executeUpdate("DELETE FROM exchanges WHERE vkid=" + vkid +";");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

//    Доделать метод,  сделать таблицу.
    public void updateDatabase(JSONObject jsonObject) {
        try {
            statement.executeUpdate(" CREATE  TABLE IF NOT EXISTS exchanges (id INT(6) AUTO_INCREMENT," +
                    " vkid VARCHAR (15), username VARCHAR(20), imgurl VARCHAR(150)," +
                    " head VARCHAR (500), body VARCHAR (1000), PRIMARY KEY (id));");
            System.out.println(jsonObject);
            statement.executeUpdate("INSERT  INTO exchanges (vkid, username, head, body,imgurl)" +
                    "VALUES (' " + jsonObject.getInt("vkid") + "'," +
                    "'" + jsonObject.getString("username") +"'," +
                    "'" + jsonObject.getString("head") + "'," +
                    "'" + jsonObject.getString("body") + "'," +
                    "'" + jsonObject.getString("imgurl")+"'"+");" );
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void updateDatabase(ArrayList<Game> games, final String table) {
        remakeTable(table);
        try {
            for (int i=0; i<games.size(); i++)
            statement.executeUpdate("INSERT INTO " + table +" (name,sale) VALUES " +
                    "('"+Game.getSQLValideString(games.get(i).getName()) + "'," +
                    "' "+games.get(i).getSale()+"');");
            statement.executeUpdate("TRUNCATE TABLE " + UPDATE_TABLE + ";");
            statement.executeUpdate("INSERT INTO " + UPDATE_TABLE + " (update_date) VALUES " +
                    "( ' " + System.currentTimeMillis() + "');");
        }
        catch (SQLException exc) {
            exc.printStackTrace();
        }
    }


    public void remakeTable( final String table) {
        try {
            statement.executeUpdate("DROP TABLE IF EXISTS "+table+";");
            statement.executeUpdate("CREATE TABLE " + table + "" +
                    " ( id INT(6) AUTO_INCREMENT, " +
                    "name VARCHAR(70), " +
                    "sale VARCHAR(50)," +
                    "PRIMARY KEY(id));");

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<Game>  getGameRows(final String table) {
        ArrayList<Game> games = new ArrayList<Game>();
        try {
            set = statement.executeQuery("SELECT * FROM "+table+";");
            while (set.next()) {
                Game game = new Game(set.getString("name"), set.getString("sale"));
                games.add(game);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }


        return games;
    }

    public ArrayList<Exchange> getExchangeRows() {
        ArrayList<Exchange> exchanges = new ArrayList<Exchange>();
        try {
            set = statement.executeQuery("SELECT * FROM exchanges;");
            while (set.next()) {
                Exchange exchange = new Exchange(
                        set.getString("vkid"),
                        set.getString("imgurl"),
                        set.getString("username"),
                        set.getString("head"),
                        set.getString("body"));
                exchanges.add(exchange);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return exchanges;
    }

}
