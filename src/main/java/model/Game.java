package model;

/**
 * Created by Ivametal on 19.03.2017.
 */
public class Game {
    private String name, sale;

    public Game (String name, String sale) {
        this.name = name;
        this.sale = sale;

    }

    public String getName() {
        return name;
    }

    public String getSale() {
        return sale;
    }


    public static String getUrl(String str) {
        String url = "";
        for (int i=0;i<str.length();i++) {
            if (str.charAt(i)=='(') {
                int j=i;
                while (str.charAt(++j)!=')')
                    url+=str.charAt(j);
                break;
            }
        }
        return url;
    }

    public static String getSQLValideString(String str) {
       return str.replaceAll("^a-zA-Z0-9_","").replaceAll("'","");
    }
}
