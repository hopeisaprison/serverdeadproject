package model;

/**
 * Created by ivametal on 07/05/17.
 */
public class Exchange {

    private String vkId;
    private String userName;
    private String headPost;
    private String bodyPost;
    private String usrImgUrl;

    public Exchange(String vkid, String usrImgUrl, String userName, String headNote, String bodyNote) {
        this.vkId = vkid;
        this.userName = userName;
        this.headPost = headNote;
        this.bodyPost = bodyNote;
        this.usrImgUrl = usrImgUrl;
    }


    public String getHeadPost() {
        return headPost;
    }

    public String getUserName() {
        return userName;
    }


    public String getBodyPost() {
        return bodyPost;
    }


    public String getVkId() {
        return vkId;
    }

    public String getUsrImgUrl() {
        return usrImgUrl;
    }

}
