package server;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.IOException;
import java.io.OutputStream;

/**
 * Created by Ivametal on 17.03.2017.
 */
public class GetContextHandler implements HttpHandler {
    JsonWriter jsonWriter;

    public GetContextHandler(JsonWriter jsonWriter) {
        this.jsonWriter = jsonWriter;
    }

    public void handle(HttpExchange httpExchange) throws IOException {
        StringBuilder builder = new StringBuilder();
        builder.append(jsonWriter.getJSONString());
        byte [] bytes = builder.toString().getBytes();
        httpExchange.sendResponseHeaders(200, bytes.length);
        OutputStream outputStream = httpExchange.getResponseBody();
        outputStream.write(bytes);
        outputStream.close();
        }
    }

