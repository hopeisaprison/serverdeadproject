package server;

import com.sun.net.httpserver.HttpContext;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import sqlbase.SQLConnector;

/**
 * Created by ivametal on 03/05/17.
 */
public class PostContextHandler implements HttpHandler {


    public void handle(HttpExchange httpExchange) throws IOException {
        InputStream inputStream = httpExchange.getRequestBody();
        String json = "";
        int ch = 0;
        while ((ch = inputStream.read()) != -1)
            json+= (char) ch;
        SQLConnector.getSqlConnector().updateTableAfterPost(json);
        System.out.println("FROM INPUT");
        String response = "Everything ok";
        httpExchange.sendResponseHeaders(200, response.length());
        OutputStream outputStream = httpExchange.getResponseBody();
        outputStream.write(response.getBytes());
        outputStream.close();
        inputStream.close();

    }
}
