package server;

import com.sun.net.httpserver.HttpContext;
import com.sun.net.httpserver.HttpServer;
import jdk.nashorn.internal.ir.debug.JSONWriter;
import sqlbase.SQLConnector;

import java.io.IOException;
import java.net.InetSocketAddress;

/**
 * Created by Ivametal on 17.03.2017.
 */
public class ServerMain extends Thread {
    private static int DEFAULT_PORT = 9999;
    private Object monitor;


    public ServerMain(Object monitor) {
        this.monitor = monitor;
    }

    public void run() {
        while (true) {

            System.out.println("ДОЖДАЛСЯ SOOOOOOOOOOOQA");
            try {
                HttpServer server = HttpServer.create();
                server.bind(new InetSocketAddress(DEFAULT_PORT), 0);

                HttpContext scThree = server.createContext("/psThree",
                        new GetContextHandler(new JsonWriter(SQLConnector.PSTHREE_TABLE)));
                HttpContext scFour = server.createContext("/psFour",
                        new GetContextHandler(new JsonWriter(SQLConnector.PSFOUR_TABLE)));
                HttpContext scXbox = server.createContext("/xbox",
                        new GetContextHandler(new JsonWriter(SQLConnector.XBOX_TABLE)));
                HttpContext scSteam = server.createContext("/steam",
                        new GetContextHandler(new JsonWriter(SQLConnector.STEAM_TABLE)));
                HttpContext post = server.createContext("/post",
                        new PostContextHandler());
                HttpContext exchanges = server.createContext("/exchanges",
                        new GetContextHandler(new JsonWriter(SQLConnector.EXCHANGE_TABLE)));

//                scThree.setAuthenticator(new Authentificator("auth"));
//                scFour.setAuthenticator(new Authentificator("auth"));
//                scXbox.setAuthenticator(new Authentificator("auth"));
//                scSteam.setAuthenticator(new Authentificator("auth"));

                server.setExecutor(null);
                server.start();
                synchronized (monitor) {
                    try {
                        monitor.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                server.stop(1);
            } catch (IOException exc) {
                exc.printStackTrace();
            }

        }
    }

}
