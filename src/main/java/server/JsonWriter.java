package server;

import model.Exchange;
import model.Game;
import org.json.JSONArray;
import org.json.JSONObject;
import sqlbase.SQLConnector;

import java.util.ArrayList;

/**
 * Created by Ivametal on 20.03.2017.
 */
public class JsonWriter  {

    private ArrayList<Game> games;
    private ArrayList<Exchange> exchanges;
    String currentTable;

    public JsonWriter(final String table) {
        currentTable = table;
        if (table.equals(SQLConnector.EXCHANGE_TABLE)) {
            exchanges = SQLConnector.getSqlConnector().getExchangeRows();
            return;
        }
        games = SQLConnector.getSqlConnector().getGameRows(table);
    }

    public String getJSONString() {
        JSONArray jsonArray = new JSONArray();
        if (!currentTable.equals(SQLConnector.EXCHANGE_TABLE)) {
            for (int i = 0; i < games.size(); i++) {
                JSONObject jsonObject = new JSONObject(games.get(i));
                jsonArray.put(jsonObject);
            }
            return jsonArray.toString();
        }
        for (int i= 0; i<exchanges.size(); i++) {
            JSONObject jsonObject = new JSONObject(exchanges.get(i));
            jsonArray.put(jsonObject);
        }
        return jsonArray.toString();

    }

}
