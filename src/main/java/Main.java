import parsers.GameParser;
import server.ServerMain;
import sqlbase.SQLConnector;

import java.util.concurrent.TimeUnit;

/**
 * Created by Ivametal on 20.03.2017.
 */
public class Main {
    public static void main(String[] args) {
        Object monitor = new Object();
        GameParser parser = new GameParser(monitor);
        while (true) {
            parser.start();
            try {
                TimeUnit.SECONDS.sleep(46800);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
