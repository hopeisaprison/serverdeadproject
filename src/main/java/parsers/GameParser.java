package parsers;

import model.Game;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import server.ServerMain;
import sqlbase.SQLConnector;

import java.io.*;
import java.util.ArrayList;

/**
 * Created by Ivametal on 19.03.2017.
 */
public class GameParser extends Thread {

    SQLConnector sql = SQLConnector.getSqlConnector();
    private Object monitor;
    private ArrayList<Game> games = new ArrayList<Game>();
    private ServerMain server;

    public GameParser (Object monitor) {
        this.monitor = monitor;
        server = new ServerMain(monitor);
    }

    public void run() {
        if (!SQLConnector.getSqlConnector().isUpdateNeeded())
            if (!server.isAlive()) {
                server.start();
                return;
            }
        else
            if (server.isAlive())
                monitor.notify();
        synchronized (monitor) {
            Document doc;
            Elements el;

            try {
                doc = Jsoup.connect("https://ps-sale.ru/").get();
                el = doc.select(".container .divlink");
                for (Element element : el) {
                    Game game = new Game(element.select(".name-holder").text(),
                            element.select(".percent-holder").text());
                    games.add(game);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            sql.updateDatabase(games, SQLConnector.PSFOUR_TABLE);

            games.clear();

            try {
                doc = Jsoup.connect("https://ps-sale.ru/ps3/").get();
                el = doc.select(".container .divlink");
                for (Element element : el) {
                    Game game = new Game(Game.getSQLValideString(element.select(".name-holder").text()),
                            element.select(".percent-holder").text());
                    games.add(game);
                }


            } catch (IOException e) {
                e.printStackTrace();
            }

            sql.updateDatabase(games, SQLConnector.PSTHREE_TABLE);

            games.clear();

            try {
                doc = Jsoup.connect("http://steamsales.rhekua.com/").get();

                el = doc.select(".tab_row");
                for (Element elem : el) {

                    Game game = new Game(elem.select(".tab_desc").select("h4").text(),
                            elem.select(".tab_discount").text());
                    games.add(game);
                }


            } catch (IOException exc) {
                exc.printStackTrace();
            }

            sql.updateDatabase(games, SQLConnector.STEAM_TABLE);

            games.clear();

            try {
                String address = "https://www.storeparser.com/en-US/xbox-one/deals/page-";
                int i = 1;

                while (true) {
                    doc = Jsoup.connect(address + i).
                            userAgent("Mozilla").get();
                    if (i == 1)
                        doc = Jsoup.connect("https://www.storeparser.com/en-US/xbox-one/deals/").
                                userAgent("Mozilla").get();
                    i++;
                    Elements elements = doc.select(".sp_spacer");
                    for (Element element : elements) {
                        if (!element.select(".sp_product_flare_container").text().equals("")) {
                            Game game = new Game(element.select(".caption").text(),
                                    element.select(".sp_product_flare_container").text());
                            games.add(game);
                        }
                    }

                }

            } catch (IOException exc) {
                exc.printStackTrace();
            }

            sql.updateDatabase(games, SQLConnector.XBOX_TABLE);


            games.clear();

            server.start();
        }
    }

}
